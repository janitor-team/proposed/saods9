Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SAOImage DS9
Upstream-Author: William Joye <wjoye@cfa.harvard.edu>
Source: http://ds9.si.edu/site/Download.html
Files-Excluded: tcl8.* tk8.* funtools tcllib tkimg tclsignal
 tktable tkcon tclxml xpa tkblt tkwin tkmpeg tkhtml1
 compilers ast tkmacosx tclzipfs libxml2 win macos ds9/win ds9/macos
 ds9/unix openssl tls

Files: *
Copyright: 1999-2018 Smithsonian Astrophysical Observatory, Cambridge, MA, USA
 1993 Association of Universities for Research in Astronomy.
 1995-2011, Mark Calabretta
 1984-2003 Free Software Foundation, Inc.
 1999-2000 Ajuba Solutions
 2002-2005 ActiveState Corporation.
License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-3'.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 Correspondence concerning SAOImage DS9 should be addressed as follows:
 .
 William Joye
 Smithsonian Astrophysical Observatory
 60 Garden St.
 Cambridge, MA 02138 USA
 wjoye@cfa.harvard.edu
 http://hea-www.harvard.edu/RD/ds9/

Files: ds9/library/tkfbox.tcl ds9/library/xmfbox.tcl
Copyright: Copyright (c) 1994-1998 Sun Microsystems, Inc.
 Copyright (c) 1998-2000 Scriptics Corporation
License: sun-BSD
 This software is copyrighted by the Regents of the University
 of California, Sun Microsystems, Inc., and other parties.  The
 following terms apply to all files associated with the
 software unless explicitly disclaimed in individual files.
 .
 The authors hereby grant permission to use, copy, modify,
 distribute, and license this software and its documentation
 for any purpose, provided that existing copyright notices are
 retained in all copies and that this notice is included
 verbatim in any distributions. No written agreement, license,
 or royalty fee is required for any of the authorized uses.
 Modifications to this software may be copyrighted by their
 authors and need not follow the licensing terms described
 here, provided that the new terms are clearly indicated on the
 first page of each file where they apply.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY
 PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR
 CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE,
 ITS DOCUMENTATION, OR ANY DERIVATIVES THEREOF, EVEN IF THE
 AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE IS PROVIDED ON
 AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE NO
 OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
 ENHANCEMENTS, OR MODIFICATIONS.
 .
 GOVERNMENT USE: If you are acquiring this software on behalf
 of the U.S. government, the Government shall have only
 "Restricted Rights" in the software and related documentation
 as defined in the Federal Acquisition Regulations (FARs) in
 Clause 52.227.19 (c) (2).  If you are acquiring the software
 on behalf of the Department of Defense, the software shall be
 classified as "Commercial Computer Software" and the
 Government shall have only "Restricted Rights" as defined in
 Clause 252.227-7013 (c) (1) of DFARs.  Notwithstanding the
 foregoing, the authors grant the U.S. Government and others
 acting in its behalf permission to use and distribute the
 software in accordance with the terms specified in this
 license.

Files: tksao/util/fdstream.hpp
Copyright: 2001 Nicolai M. Josuttis
License: Expat
 Permission to copy, use, modify, sell and distribute this
 software is granted provided this copyright notice appears in
 all copies.  This software is provided "as is" without express
 or implied warranty, and with no claim as to its suitability
 for any purpose.

Files: tksao/util/FlexLexer.h
Copyright: Kent Williams and Tom Epperly.
License: BSD-3-clause
 This code is derived from software contributed to Berkeley by
 Kent Williams and Tom Epperly.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 Neither the name of the University nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE.

Files: */tclconfig/install-sh tclconfig/install-sh
Copyright: 1994 X Consortium
License: MIT/X11
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.
Comment: FSF changes to this file are in the public domain.

Files: tksao/*/*parser.[CH]
Copyright: 1984-2006 Free Software Foundation, Inc.
 Jason Tang (tang@jtang.org)
License: GPL-2+
Note: As a special exception, you may create a larger work that
 contains part or all of the Bison parser skeleton and distribute that
 work under terms of your choice, so long as that work isn't itself a
 parser generator using the skeleton or a modified version thereof as
 a parser skeleton.  Alternatively, if you modify or redistribute the
 parser skeleton itself, you may (at your option) remove this special
 exception, which will cause the skeleton and the resulting Bison
 output files to be licensed under the GNU General Public License
 without this special exception.

Files: taccle/* fickle/*
Copyright: Jason Tang (tang@jtang.org)
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.

Files: debian/*
Copyright: not applicable
License: public-domain
 This package was debianized by Justin Pryzby <justinpryzby@users.sf.net> on
 Tue,  4 Jan 2005 21:53:52 -0500.  The Debian .diff is hereby placed
 into the public domain.
 .
 Based on Justin's diff, the debianization is completely rewritten for
 the upstream version 6.2 by Ole Streicher <debian@liska.ath.cx>.
 Contributions came from the Gentoo packaging by Sebastien Fabbro
 <bicatali@gentoo.org>.
